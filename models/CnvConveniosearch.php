<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CnvConvenio;

/**
 * CnvConvenioSearch represents the model behind the search form about `app\models\CnvConvenio`.
 */
class CnvConvenioSearch extends CnvConvenio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_CONVENIO', 'ID_TIPO_CONVENIO', 'ID_ESTADO_CONVENIO', 'NUMERO_DECRETO', 'VIGENCIA'], 'integer'],
            [['ID_COORDINADOR_CONVENIO', 'NOMBRE_CONVENIO', 'FECHA_INICIO', 'FECHA_TERMINO', 'FECHA_FIRMA', 'FECHA_DECRETO', 'DESCRIPCION', 'VIGENTE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CnvConvenio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID_CONVENIO' => $this->ID_CONVENIO,
            'ID_TIPO_CONVENIO' => $this->ID_TIPO_CONVENIO,
            'ID_ESTADO_CONVENIO' => $this->ID_ESTADO_CONVENIO,
            'FECHA_INICIO' => $this->FECHA_INICIO,
            'FECHA_TERMINO' => $this->FECHA_TERMINO,
            'FECHA_FIRMA' => $this->FECHA_FIRMA,
            'FECHA_DECRETO' => $this->FECHA_DECRETO,
            'NUMERO_DECRETO' => $this->NUMERO_DECRETO,
            'VIGENCIA' => $this->VIGENCIA,
        ]);

        $query->andFilterWhere(['like', 'ID_COORDINADOR_CONVENIO', $this->ID_COORDINADOR_CONVENIO])
            ->andFilterWhere(['like', 'NOMBRE_CONVENIO', $this->NOMBRE_CONVENIO])
            ->andFilterWhere(['like', 'DESCRIPCION', $this->DESCRIPCION])
            ->andFilterWhere(['like', 'VIGENTE', $this->VIGENTE]);

        return $dataProvider;
    }
}
