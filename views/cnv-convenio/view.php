<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CnvConvenio */

$this->title = $model->ID_CONVENIO;
$this->params['breadcrumbs'][] = ['label' => 'Cnv Convenios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnv-convenio-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID_CONVENIO], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID_CONVENIO], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_CONVENIO',
            'ID_TIPO_CONVENIO',
            'ID_COORDINADOR_CONVENIO',
            'ID_ESTADO_CONVENIO',
            'NOMBRE_CONVENIO',
            'FECHA_INICIO',
            'FECHA_TERMINO',
            'FECHA_FIRMA',
            'FECHA_DECRETO',
            'NUMERO_DECRETO',
            'DESCRIPCION',
            'VIGENTE',
            'VIGENCIA',
        ],
    ]) ?>

</div>
