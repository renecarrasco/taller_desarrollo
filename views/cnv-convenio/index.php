<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CnvConvenioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cnv Convenios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnv-convenio-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cnv Convenio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID_CONVENIO',
            'ID_TIPO_CONVENIO',
            'ID_COORDINADOR_CONVENIO',
            'ID_ESTADO_CONVENIO',
            'NOMBRE_CONVENIO',
            // 'FECHA_INICIO',
            // 'FECHA_TERMINO',
            // 'FECHA_FIRMA',
            // 'FECHA_DECRETO',
            // 'NUMERO_DECRETO',
            // 'DESCRIPCION',
            // 'VIGENTE',
            // 'VIGENCIA',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
