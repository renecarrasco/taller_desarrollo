<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CnvConvenio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cnv-convenio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_CONVENIO')->textInput() ?>

    <?= $form->field($model, 'ID_TIPO_CONVENIO')->textInput() ?>

    <?= $form->field($model, 'ID_COORDINADOR_CONVENIO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ID_ESTADO_CONVENIO')->textInput() ?>

    <?= $form->field($model, 'NOMBRE_CONVENIO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FECHA_INICIO')->textInput() ?>

    <?= $form->field($model, 'FECHA_TERMINO')->textInput() ?>

    <?= $form->field($model, 'FECHA_FIRMA')->textInput() ?>

    <?= $form->field($model, 'FECHA_DECRETO')->textInput() ?>

    <?= $form->field($model, 'NUMERO_DECRETO')->textInput() ?>

    <?= $form->field($model, 'DESCRIPCION')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'VIGENTE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'VIGENCIA')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
